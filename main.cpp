#include <iterator>
#include "deep_const.h"
#include <iostream>
#include <memory>

using namespace deep_const;

const char* types() {
  return __PRETTY_FUNCTION__;
}

template <class... Args>
const char* types() {
  return __PRETTY_FUNCTION__;
}

template <class... Args>
const char* types(Args&&...) {
  return types<Args&&...>();
}

#define P(x) #x ":\t" << (x)
#define CHECK(type, expr) static_assert(std::is_same<decltype(expr), type>::value, #expr " is not of type " #type)

struct Foo {
  Foo& get_this() {
    return *this;
  }
  const Foo& get_this() const {
    return *this;
  }
};
struct Bar : public Foo {};

template class dc_wrap<int*, true>;
template class dc_wrap<int*, false>;

int main() {
  int i = 0;
  dc_wrap<int*> p0 = &i;
  dc_wrap<int*> p1 = p0;
  const dc_wrap<int*> p2 = p0;
  //const dc_wrap<int*> p3 = p2; // Should be valid, but is not
  //dc_wrap<int*> p4 = p2; // Not valid
  dc_wrap<int*,true> p5 = p0;
  dc_wrap<int*,true> p6 = p2;
  p5 = static_cast<int const*>(&i);
  std::cout << P(types<decltype(p0)>()) << std::endl;
  std::cout << P(types<decltype(p5)>()) << std::endl;
  std::cout << P(types<decltype(p2 + 1)>()) << std::endl;
  std::cout << P(types<decltype(p2 - 1)>()) << std::endl;
  std::cout << P(types<decltype(p0 + 1)>()) << std::endl;
  std::cout << P(types<decltype(p0 - 1)>()) << std::endl;
  std::cout << P(types<decltype(1+p0)>()) << std::endl;
  std::cout << P(types<decltype(1+p2)>()) << std::endl;
  std::cout << P(types<decltype(*p0)>()) << std::endl;
  std::cout << P(types<decltype(*p2)>()) << std::endl;
  std::cout << P(types<decltype(p0[0])>()) << std::endl;
  std::cout << P(types<decltype(p2[0])>()) << std::endl;
  std::cout << P(types<decltype(p0.operator->())>()) << std::endl;
  std::cout << P(types<decltype(p2.operator->())>()) << std::endl;
  std::cout << P(types<decltype(p0.get())>()) << std::endl;
  std::cout << P(types<decltype(p2.get())>()) << std::endl;
  std::cout << P(types<decltype(p0 += 1)>()) << std::endl;
  std::cout << P(types<decltype(p5 += 1)>()) << std::endl;
  std::cout << P(types<decltype(p0 -= 1)>()) << std::endl;
  std::cout << P(types<decltype(p5 -= 1)>()) << std::endl;
  //std::cout << P(types<decltype(std::declval<Bar*&>() = (std::declval<Foo*&>()))>()) << std::endl;
  std::cout << P(types<decltype(std::declval<Foo*&>() = (std::declval<Bar*>()))>()) << std::endl;
  //std::cout << P(types<decltype(std::declval<dc_wrap<Bar*>&>() = (std::declval<dc_wrap<Foo*>&>()))>()) << std::endl;
  std::cout << P(types<decltype(std::declval<dc_wrap<Foo*>&>() = (std::declval<dc_wrap<Bar*>&>()))>()) << std::endl;
  std::cout << P(types<decltype(std::declval<dc_wrap<int*>&>() = (std::declval<dc_wrap<int*>&&>()))>()) << std::endl;
  std::cout << P(types<decltype(std::declval<const   const_dc_wrap<Bar*>&>()->get_this())>()) << std::endl;
  std::cout << P(types<decltype(std::declval<        const_dc_wrap<Bar*>&>()->get_this())>()) << std::endl;
  std::cout << P(types<decltype(std::declval<const mutable_dc_wrap<Bar*>&>()->get_this())>()) << std::endl;
  std::cout << P(types<decltype(std::declval<      mutable_dc_wrap<Bar*>&>()->get_this())>()) << std::endl;
  return 0;
}
