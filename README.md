# deep_const.h

This library provide a wrapper for pointers that allows to add
"deep-const"ness semantic to the pointer.

The "deep-const"ness could be understood like this:
  - if the wrapped pointer is mutable, it behaves the same as a raw pointer
  - if the wrapped pointer is const, the pointee is also const

## Interface

```cpp
namespace deep_const {

  // Wrapper to add the "deep-const"ness semantic to pointer_t
  template <class pointer_t, bool immutable = false> class dc_wrap;

  // typedef to the mutable wrapper
  template <class T> using mutable_dc_wrap = dc_wrap<T, false>;

  // typedef to the const wrapper
  template <class T> using   const_dc_wrap = dc_wrap<T, true>;

  // typedef to recursively wrap pointer types
  template <class T> using deep_const_t = /* implementation defined */;

  // transform a pointer object into recursively wrapped pointer object
  // same as deep_const_t but for objects instead of types
  template <class T>
  deep_const_t<T> deep_const(T&&);
}
```



Appart the constness, a `dc_wrap<T*>` has exactly the same semantic as a plain `T*`
So what you can do on `T*`, you can also do it on a `dc_wrap<T*>`.

```cpp
int data[16];
mutable_dc_wrap<int*> p = &data[0];
int i;
i = *p;
i = p[1];
p[2] = 3;
mutable_dc_wrap<int*> p1 = p + 2;
```



Here are the rules for dereferencing a `dc_wrap<T*>`:
  - dereferencing a `const   const_dc_wrap<T*>` gives a `const T&`
  - dereferencing a `        const_dc_wrap<T*>` gives a `const T&`
  - dereferencing a `const mutable_dc_wrap<T*>` gives a `const T&`
  - dereferencing a `      mutable_dc_wrap<T*>` gives a `      T&`



The cast rules have changed a bit:
You cannot assign or convert a `const_dc_wrap<T*>` or a `const mutable_dc_wrap<T*>` to a `mutable_dc_wrap<T*>`.
This also means the following code is actually incorrect:

```cpp
const_dc_wrap<int*> const_p;
const mutable_dc_wrap<int*> p = const_p; // ERROR: cannot construct a "mutable_dc_wrap<int*>" from a "const_dc_wrap<int*>"
```



However, it is possible to cast a `const_dc_wrap<T*>&` into a `const mutable_dc_wrap<T*>&`
This makes the following code valid:

```cpp
void foo(const mutable_dc_wrap<int*>& p);

const_dc_wrap<int*> const_p;
foo(const_p); // this call is valid!
```



`deep_const_t<T>` wraps recursively the type T.
eg: `deep_const_t<T**>` gives `mutable_dc_wrap<mutable_dc_wrap<T*>*>`

The `deep_const` has the same meaning as `deep_const_t`, but works on objects
A naive implementation of `deep_const` could be:

```cpp
template <class T>
deep_const_t<T*> deep_const(T* p) {
  return deep_const_t<T*>(reinterpret_cast<deep_const_t<T>*>(p));
}
```
