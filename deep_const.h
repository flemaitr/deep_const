////////////////////////////////////////////////////////////////////////////////
//                                                                            //
// deep_const.h                                                               //
//                                                                            //
// This file provide a wrapper for pointers that allows to add                //
// "deep-const"ness semantic to the pointer.                                  //
//                                                                            //
// The "deep-const"ness could be understood like this:                        //
// if the wrapped pointer is mutable, it behaves the same as a raw pointer    //
// if the wrapped pointer is const, the pointee is also const                 //
//                                                                            //
// This file is licensed under the MIT license.                               //
// License can be found at the end of this file                               //
////////////////////////////////////////////////////////////////////////////////
//
// This single-header library provides the following interface:
#ifdef DEEP_CONST_CODE_EXAMPLE
namespace deep_const {

  // Wrapper to add the "deep-const"ness semantic to pointer_t
  template <class pointer_t, bool immutable = false> class dc_wrap;

  // typedef to the mutable wrapper
  template <class T> using mutable_dc_wrap = dc_wrap<T, false>;

  // typedef to the const wrapper
  template <class T> using   const_dc_wrap = dc_wrap<T, true>;

  // typedef to recursively wrap pointer types
  template <class T> using deep_const_t = /* implementation defined */;

  // transform a pointer object into recursively wrapped pointer object
  // same as deep_const_t but for objects instead of types
  template <class T>
  deep_const_t<T> deep_const(T&&);
}
#endif
//
//
//
// Appart the constness, a "dc_wrap<T*>" has exactly the same semantic as a plain "T*"
// So what you can do on "T*", you can also do it on a "dc_wrap<T*>".
#ifdef DEEP_CONST_CODE_EXAMPLE

int data[16];
mutable_dc_wrap<int*> p = &data[0];
int i;
i = *p;
i = p[1];
p[2] = 3;
mutable_dc_wrap<int*> p1 = p + 2;

#endif
//
//
//
// Here are the rules for dereferencing a "dc_wrap<T*>":
// - dereferencing a "const   const_dc_wrap<T*>" gives a "const T&"
// - dereferencing a "        const_dc_wrap<T*>" gives a "const T&"
// - dereferencing a "const mutable_dc_wrap<T*>" gives a "const T&"
// - dereferencing a "      mutable_dc_wrap<T*>" gives a "      T&"
// 
//
//
// The cast rules have changed a bit:
// You cannot assign or convert a "const_dc_wrap<T*>" or a "const mutable_dc_wrap<T*>" to a "mutable_dc_wrap<T*>".
// This also means the following code is actually incorrect:
#ifdef DEEP_CONST_CODE_EXAMPLE

const_dc_wrap<int*> const_p;
const mutable_dc_wrap<int*> p = const_p; // ERROR: cannot construct a "mutable_dc_wrap<int*>" from a "const_dc_wrap<int*>"

#endif
//
//
//
// However, it is possible to cast a "const_dc_wrap<T*>&" into a "const mutable_dc_wrap<T*>&"
// This makes the following code valid:
#ifdef DEEP_CONST_CODE_EXAMPLE

void foo(const mutable_dc_wrap<int*>& p);

const_dc_wrap<int*> const_p;
foo(const_p); // this call is valid!

#endif
//
//
//
// "deep_const_t<T>" wraps recursively the type T.
// eg: "deep_const_t<T**>" gives "mutable_dc_wrap<mutable_dc_wrap<T*>*>"
//
// The "deep_const" has the same meaning as "deep_const_t", but works on objects
// A naive implementation of "deep_const" could be:
#ifdef DEEP_CONST_CODE_EXAMPLE
template <class T>
deep_const_t<T*> deep_const(T* p) {
  return deep_const_t<T*>(reinterpret_cast<deep_const_t<T>*>(p));
}
#endif



#ifndef DEEP_CONST_H
#define DEEP_CONST_H
#include <cstddef>
#include <cstdint>
#include <utility>
#include <type_traits>

/**
 * Forward declarations of some std types
 * Allow to not include <memory> and <iterator>
 */
namespace std {
  //template <class T, class Deleter>
  //class unique_ptr;

  //template <class T>
  //class shared_ptr;

  struct random_access_iterator_tag;
}

/**
 * Namespace of the deep-const wrapper
 */
namespace deep_const {

  // forward-declare the wrapper
  template <class T, bool immutable = false>
  class dc_wrap;

  /**
   * Some helpers
   */
  namespace details {
    // A true declval (maybe useless)
    template <class T>
    T declval();

    /**
     * Transform a type (chain of raw/smart pointers) into a deeper-const one
     */
    template <class T>
    struct deepen_const_s {
      using type = const T;
    };

    template <class T>
    struct deepen_const_s<T&> {
      using type = const typename deepen_const_s<T>::type&;
    };

    template <class T>
    struct deepen_const_s<T&&> {
      using type = const typename deepen_const_s<T>::type&&;
    };

    template <class T>
    struct deepen_const_s<T*> {
      using type = const T*const;
    };

    //template <class T, class Deleter>
    //struct deepen_const_s<std::unique_ptr<T, Deleter>> {
    //  using type = const std::unique_ptr<const T, Deleter>;
    //};

    //template <class T>
    //struct deepen_const_s<std::shared_ptr<T>> {
    //  using type = const std::shared_ptr<const T>;
    //};

    // typedef to deepen the constness of a type
    template <class T>
    using deepen_const_t = typename std::remove_cv<typename deepen_const_s<T>::type>::type;

    // Convert an object into a deeper-const one
    template <class T>
    deepen_const_t<T&&> deepen_const(T&& p) {
      return static_cast<deepen_const_t<T&&>>(std::forward<T>(p));
    }

    /**
     * Type trait to detect the wrapper
     */
    template <class T>         struct is_dc_wrap                : public std::false_type {};
    template <class T>         struct is_dc_wrap<const T>       : public is_dc_wrap<T> {};
    template <class T>         struct is_dc_wrap<      T& >     : public is_dc_wrap<T> {};
    template <class T>         struct is_dc_wrap<      T&&>     : public is_dc_wrap<T> {};
    template <class P, bool b> struct is_dc_wrap<dc_wrap<P, b>> : public std::true_type {};

    // get the pointer type of the wrapper (const-aware)
    template <class WRAP>
    using wrapper_inner_type = decltype(declval<WRAP>().get());

    /**
     * valid_implicit_cast
     *
     * Check if FROM type can be implicitely converted into TO type (const-aware)
     * both should be wrapper types
     */
    template <class FROM, class TO, typename std::enable_if<is_dc_wrap<FROM>::value && is_dc_wrap<TO>::value && std::is_convertible<wrapper_inner_type<FROM>, wrapper_inner_type<TO>>::value>::type* = nullptr>
    constexpr bool valid_implicit_cast_f(int=0) { return true; }
    template <class...>
    constexpr bool valid_implicit_cast_f(...) { return false; }

    template <class FROM, class TO>
    constexpr bool valid_implicit_cast() { return valid_implicit_cast_f<FROM, TO>(0); }

    /**
     * valid_cast
     *
     * Check if FROM type can be either implicitely or explicitely converted into TO type (const-aware)
     * both should be wrapper types
     */
    template <class FROM, class TO, typename std::enable_if<is_dc_wrap<FROM>::value && is_dc_wrap<TO>::value>::type* = nullptr, decltype(static_cast<wrapper_inner_type<TO>>(declval<wrapper_inner_type<FROM>>()), nullptr) = nullptr>
    constexpr bool valid_cast_f(int=0) { return true; }
    template <class...>
    constexpr bool valid_cast_f(...) { return false; }

    template <class FROM, class TO>
    constexpr bool valid_cast() { return valid_cast_f<FROM, TO>(0); }

    /**
     * valid_explicit_cast
     *
     * Check if FROM type can be explicitely but not implicitely converted into TO type (const-aware)
     * both should be wrapper types
     */
    template <class FROM, class TO>
    constexpr bool valid_explicit_cast() { return valid_cast<FROM, TO>() && !valid_implicit_cast<FROM, TO>(); }


    /**
     * Check if construct a TO object from a FROM object is valid (const-aware)
     * both should be wrapper types
     */
    template <class FROM, class TO>
    constexpr bool valid_implicit_constructor() {
      return valid_implicit_cast<FROM, TO>();
    }
    template <class FROM, class TO>
    constexpr bool valid_explicit_constructor() {
      return valid_explicit_cast<FROM, TO>();
    }

    /**
     * Check if conversion operator from FROM object to TO object is valid (const-aware)
     * both should be wrapper types
     */
    template <class FROM, class TO>
    constexpr bool valid_implicit_conversion() {
      return valid_implicit_cast<FROM, TO>() && !valid_cast<FROM, typename std::remove_cv<TO>::type>();
    }
    template <class FROM, class TO>
    constexpr bool valid_explicit_conversion() {
      return valid_explicit_cast<FROM, TO>() && !valid_cast<FROM, typename std::remove_cv<TO>::type>();
    }

    /**
     * Deep-const a type
     * insert a wrapper at each pointer level
     */
    template <class T> struct deep_const_s { using type = T; };
    template <class T> struct deep_const_s<      T& > { using type =         typename deep_const_s<T>::type& ; };
    template <class T> struct deep_const_s<      T&&> { using type =         typename deep_const_s<T>::type&&; };
    template <class T> struct deep_const_s<const T  > { using type =   const typename deep_const_s<T>::type  ; };
    template <class T> struct deep_const_s<      T* > { using type = dc_wrap<typename deep_const_s<T>::type*>; };

    // typedef
    template <class T> using deep_const_t = typename deep_const_s<T>::type;


    ///**
    // * Cast any pointer type into any other pointer type (whatever the constness is)
    // * The "deep-const"ed input should be (explicitly) convertible into the "deep-const"ed output
    // * This should be useful only for smart pointers
    // */
    //template <class TO, class FROM, decltype(static_cast<deep_const_t<TO>>(declval<deep_const_t<FROM>>()))* = nullptr>
    //TO unsafe_reinterpret_const_cast(FROM from) noexcept {
    //  union {
    //    void* dummy;
    //    FROM from;
    //    TO to;
    //  } caster;
    //  caster.from = from;
    //  return caster.to;
    //}
    //template <class TO>
    //TO unsafe_reinterpret_const_cast(...) noexcept {
    //  static_assert(sizeof(TO) == 0, "Invalid cast");
    //  return TO{};
    //}

    /**
     * Deep-const an object
     * insert a wrapper at each pointer level
     */
    template <class T>
    deep_const_t<typename std::remove_cv<typename std::remove_reference<T>::type>::type> deep_const(T&& t) noexcept {
      using wrapper_t = deep_const_t<typename std::remove_cv<typename std::remove_reference<T>::type>::type>;
      using     ptr_t = wrapper_inner_type<wrapper_t>;
      return wrapper_t(reinterpret_cast<ptr_t>(std::forward<T>(t)));
    }

  } // namespace details

  // Put deep_const_t typedef and deep_const function into deep_const namespace
  using typename details::deep_const_t;
  using          details::deep_const;

  // Typedef to put unambiguous names on the 2 wrapper types
  template <class T> using mutable_dc_wrap = dc_wrap<T, false>;
  template <class T> using   const_dc_wrap = dc_wrap<T, true>;


  /**
   * Immutable wrapper
   */
  template <class T>
  class dc_wrap<T*, true> {
    private:
      // helper typedefs
      using self_t = dc_wrap<T*, true>;
      using inner_mutable_t = T*;
      using inner_const_t = details::deepen_const_t<T*>;
      using inner_t = inner_const_t;

    private:
      // pointer member
      inner_t p = nullptr;

    public:
      // Public Typedefs
      using mutable_t = dc_wrap<T*, false>;
      using const_t   = dc_wrap<T*, true>;

      using difference_type   = std::ptrdiff_t;
      using value_type        = details::deepen_const_t<T>;
      using element_type      = details::deepen_const_t<T>;
      using pointer           = self_t;
      using reference         = details::deepen_const_t<T&>;
      using iterator_category = std::random_access_iterator_tag;

    public:
      // Get inner pointer
      inner_const_t get() const noexcept {
        return this->p;
      }

    public:
      /**
       * Constructors
       */
      // regular constructors
      dc_wrap() = default;
      dc_wrap(const dc_wrap&) = default;
      //dc_wrap(inner_t p) noexcept : p(p) {}
      dc_wrap(inner_const_t p) noexcept : p(p) {}

      // Conversion constructors from pointer
      template <class PTR, typename std::enable_if<!details::is_dc_wrap<PTR>::value &&  std::is_convertible<PTR, inner_const_t>::value>::type* = nullptr>
      dc_wrap(PTR ptr) noexcept : p(ptr) {}
      template <class PTR, typename std::enable_if<!details::is_dc_wrap<PTR>::value && !std::is_convertible<PTR, inner_const_t>::value>::type* = nullptr, decltype(static_cast<inner_const_t>(details::declval<PTR>()))* = nullptr>
      explicit dc_wrap(PTR ptr) noexcept : p(static_cast<inner_t>(ptr)) {}
      
      // Conversion constructors from wrapper
      template <class WRAP, decltype(nullptr) = typename std::enable_if<details::valid_implicit_constructor<WRAP, dc_wrap>(), decltype(nullptr)>::type()>
      dc_wrap(WRAP&& wrap) noexcept : p(wrap.get()) {}
      template <class WRAP, decltype(nullptr) = typename std::enable_if<details::valid_explicit_constructor<WRAP, dc_wrap>(), decltype(nullptr)>::type()>
      explicit dc_wrap(WRAP&& wrap, decltype(nullptr)=nullptr) noexcept : p(static_cast<inner_t>(wrap.get())) {}

      /**
       * Destructor
       */
      ~dc_wrap() = default;

    public:
      /**
       * Assignment operators
       */
      // regular assignments
      dc_wrap& operator=(inner_const_t p) noexcept {
        return *this = dc_wrap(p);
      }
      dc_wrap& operator=(const dc_wrap& dc) noexcept {
        this->p = dc.p;
        return *this;
      }

      // conversion assignment operator from pointer (implicit conversion)
      template <class PTR, typename std::enable_if<!details::is_dc_wrap<PTR>::value &&  std::is_convertible<PTR, inner_const_t>::value>::type* = nullptr>
      dc_wrap& operator=(PTR ptr) noexcept {
        this->p = ptr;
        return *this;
      }

      // conversion assignment operator from wrapper (implicit conversion)
      template <class WRAP, typename std::enable_if<details::valid_implicit_cast<WRAP, dc_wrap&>()>::type* = nullptr>
      dc_wrap& operator=(WRAP&& wrap) noexcept {
        this->p = wrap.get();
        return *this;
      }

    public:
      /**
       * Conversion operators
       */
      // Conversion to inner pointer type
      operator inner_const_t() const noexcept {
        return this->get();
      }
      
      // Conversion to pointer
      template <class PTR, typename std::enable_if<!details::is_dc_wrap<PTR>::value &&  std::is_convertible<PTR, inner_const_t>::value>::type* = nullptr>
      operator PTR() const noexcept {
        return this->get();
      }
      template <class PTR, typename std::enable_if<!details::is_dc_wrap<PTR>::value && !std::is_convertible<PTR, inner_const_t>::value>::type* = nullptr, decltype(static_cast<PTR>(details::declval<inner_const_t>()))* = nullptr>
      explicit operator PTR() const noexcept {
        return static_cast<PTR>(this->get());
      }


      // Implicit conversion to wrapper reference
      template <class WRAP, int = typename std::enable_if<details::valid_implicit_conversion<      dc_wrap&,  WRAP& >(), int>::type(0)>
      operator WRAP&  ()       & {
        return reinterpret_cast<WRAP& >(*this);
      }
      template <class WRAP, int = typename std::enable_if<details::valid_implicit_conversion<      dc_wrap&&, WRAP&&>(), int>::type(0)>
      operator WRAP&& ()       && {
        return reinterpret_cast<WRAP&&>(*this);
      }
      template <class WRAP, int = typename std::enable_if<details::valid_implicit_conversion<const dc_wrap&,  WRAP& >(), int>::type(0)>
      operator WRAP&  () const &  {
        return reinterpret_cast<WRAP& >(*this);
      }
      template <class WRAP, int = typename std::enable_if<details::valid_implicit_conversion<const dc_wrap&&, WRAP&&>(), int>::type(0)>
      operator WRAP&& () const && {
        return reinterpret_cast<WRAP&&>(*this);
      }

      // Explicit conversion to wrapper reference
      template <class WRAP, typename std::enable_if<details::valid_explicit_conversion<      dc_wrap&,  WRAP& >()>::type* = nullptr>
      explicit operator WRAP&  ()       & {
        return reinterpret_cast<WRAP& >(*this);
      }
      template <class WRAP, typename std::enable_if<details::valid_explicit_conversion<      dc_wrap&&, WRAP&&>()>::type* = nullptr>
      explicit operator WRAP&& ()       && {
        return reinterpret_cast<WRAP&&>(*this);
      }
      template <class WRAP, typename std::enable_if<details::valid_explicit_conversion<const dc_wrap&,  WRAP& >()>::type* = nullptr>
      explicit operator WRAP&  () const &  {
        return reinterpret_cast<WRAP& >(*this);
      }
      template <class WRAP, typename std::enable_if<details::valid_explicit_conversion<const dc_wrap&&, WRAP&&>()>::type* = nullptr>
      explicit operator WRAP&& () const && {
        return reinterpret_cast<WRAP&&>(*this);
      }

    public:
      /**
       * Dereferecing operators
       */
      auto operator*() const noexcept -> decltype((*(this->get()))) {
        return *(this->get());
      }

      auto operator[](std::ptrdiff_t n) const noexcept -> decltype((this->get()[n])) {
        return this->get()[n];
      }

      auto operator->() const noexcept -> decltype(this->get()) {
        return this->get();
      }

      /**
       * Pointer arithmetic
       */
      dc_wrap operator+(std::ptrdiff_t n) const noexcept {
        return dc_wrap(this->get() + n);
      }

      friend dc_wrap operator+(std::ptrdiff_t n, const dc_wrap& dc) noexcept {
        return dc + n;
      }

      dc_wrap operator-(std::ptrdiff_t n) const noexcept {
        return dc_wrap(this->get() - n);
      }

      dc_wrap& operator+=(std::ptrdiff_t n) noexcept {
        this->p += n;
        return *this;
      }
      dc_wrap& operator-=(std::ptrdiff_t n) noexcept {
        this->p -= n;
        return *this;
      }

      std::ptrdiff_t operator-(const dc_wrap& dc) const noexcept {
        return dc.get() - this->get();
      }

      /**
       * Comparison ops
       */
      bool operator==(const dc_wrap& dc) const noexcept {
        return this->get() == dc.get();
      }
      bool operator!=(const dc_wrap& dc) const noexcept {
        return this->get() != dc.get();
      }
      bool operator<(const dc_wrap& dc) const noexcept {
        return this->get() < dc.get();
      }
      bool operator<=(const dc_wrap& dc) const noexcept {
        return this->get() <= dc.get();
      }
      bool operator>(const dc_wrap& dc) const noexcept {
        return this->get() > dc.get();
      }
      bool operator>=(const dc_wrap& dc) const noexcept {
        return this->get() >= dc.get();
      }

  }; // class dc_wrap<T, true>



  /**
   * Mutable wrapper
   */
  template <class T>
  class dc_wrap<T*, false> {
    private:
      // helper typedefs
      using self_t = dc_wrap<T*, false>;
      using inner_t = T*;
      using inner_mutable_t = T*;
      using inner_const_t = details::deepen_const_t<T*>;

    private:
      // pointer member
      inner_t p = nullptr;

    public:
      // Public Typedefs
      using mutable_t = dc_wrap<T*, false>;
      using const_t   = dc_wrap<T*, true>;

      using difference_type   = std::ptrdiff_t;
      using value_type        = T;
      using element_type      = T;
      using pointer           = self_t;
      using reference         = T&;
      using iterator_category = std::random_access_iterator_tag;

    public:
      // Get inner pointer
      inner_const_t get() const noexcept {
        return details::deepen_const(this->p);
      }
      inner_mutable_t get() noexcept {
        return this->p;
      }

    public:
      /**
       * Constructors
       */
      // regular constructors
      dc_wrap() = default;
      dc_wrap(      dc_wrap&  dc) noexcept : p(dc.get()) {}
      dc_wrap(      dc_wrap&& dc) noexcept : p(dc.get()) {}

      // avoid constness-leaking
      dc_wrap(const dc_wrap&  dc) = delete;
      dc_wrap(const const_t&  dc) = delete;
      dc_wrap(inner_const_t   p)  = delete;

      // Conversion constructors from pointer
      template <class PTR, typename std::enable_if<!details::is_dc_wrap<PTR>::value &&  std::is_convertible<PTR, inner_mutable_t>::value>::type* = nullptr>
      dc_wrap(PTR ptr) noexcept : p(ptr) {}
      template <class PTR, typename std::enable_if<!details::is_dc_wrap<PTR>::value && !std::is_convertible<PTR, inner_mutable_t>::value>::type* = nullptr, decltype(static_cast<inner_mutable_t>(details::declval<PTR>()))* = nullptr>
      explicit dc_wrap(PTR ptr) noexcept : p(static_cast<inner_t>(ptr)) {}

      // Conversion constructors from wrapper
      template <class WRAP, typename std::enable_if<details::valid_implicit_cast<WRAP, dc_wrap>()>::type* = nullptr>
      dc_wrap(WRAP&& wrap) noexcept : p(const_cast<inner_t>(wrap.get())) {}
      template <class WRAP, typename std::enable_if<details::valid_explicit_cast<WRAP, dc_wrap>()>::type* = nullptr>
      explicit dc_wrap(WRAP&& wrap) noexcept : p(const_cast<inner_t>(wrap.get())) {}


      /**
       * Destructor
       */
      ~dc_wrap() = default;

    public:
      /**
       * Assignment operators
       */
      // regular assignments
      dc_wrap& operator=(dc_wrap&  dc) noexcept {
        this->p = dc.get();
        return *this;
      }
      dc_wrap& operator=(dc_wrap&& dc) noexcept {
        this->p = dc.get();
        return *this;
      }

      // avoid constness-leaking
      dc_wrap& operator=(inner_const_t  ptr) = delete;
      dc_wrap& operator=(const dc_wrap&  dc) = delete;

      // conversion assignment operator from pointer (implicit conversion)
      template <class PTR, typename std::enable_if<!details::is_dc_wrap<PTR>::value &&  std::is_convertible<PTR, inner_mutable_t>::value>::type* = nullptr>
      dc_wrap& operator=(PTR ptr) noexcept {
        this->p = ptr;
        return *this;
      }

      // conversion assignment operator from wrapper (implicit conversion)
      template <class WRAP, typename std::enable_if<details::valid_implicit_cast<WRAP, dc_wrap&>()>::type* = nullptr>
      dc_wrap& operator=(WRAP&& wrap) noexcept {
        this->p = const_cast<inner_t>(wrap.get());
        return *this;
      }

    public:
      /**
       * Conversion operators
       */
      // Conversion to inner pointer type
      operator inner_const_t() const noexcept {
        return this->get();
      }
      operator inner_mutable_t() noexcept {
        return this->get();
      }

      // Implicit conversion to pointer
      template <class PTR, typename std::enable_if<!details::is_dc_wrap<PTR>::value &&  std::is_convertible<PTR, inner_const_t>::value>::type* = nullptr>
      operator PTR() const noexcept {
        return this->get();
      }
      template <class PTR, typename std::enable_if<!details::is_dc_wrap<PTR>::value &&  std::is_convertible<PTR, inner_mutable_t>::value>::type* = nullptr>
      operator PTR() noexcept {
        return this->get();
      }

      // Explicit conversion to pointer
      template <class PTR, typename std::enable_if<!details::is_dc_wrap<PTR>::value && !std::is_convertible<PTR, inner_const_t>::value>::type* = nullptr, decltype(static_cast<PTR>(details::declval<inner_const_t>()))* = nullptr>
      explicit operator PTR() const noexcept {
        return static_cast<PTR>(this->get());
      }
      template <class PTR, typename std::enable_if<!details::is_dc_wrap<PTR>::value && !std::is_convertible<PTR, inner_mutable_t>::value>::type* = nullptr, decltype(static_cast<PTR>(details::declval<inner_mutable_t>()))* = nullptr>
      explicit operator PTR() noexcept {
        return static_cast<PTR>(this->get());
      }

      // Implicit conversion to wrapper reference
      template <class WRAP, typename std::enable_if<details::valid_implicit_cast<      dc_wrap&,  WRAP& >() && !details::valid_implicit_cast<      dc_wrap&,  typename std::remove_cv<WRAP>::type>()>::type* = nullptr>
      operator WRAP&  ()       & {
        return reinterpret_cast<WRAP& >(*this);
      }
      template <class WRAP, typename std::enable_if<details::valid_implicit_cast<      dc_wrap&&, WRAP&&>() && !details::valid_implicit_cast<      dc_wrap&&, typename std::remove_cv<WRAP>::type>()>::type* = nullptr>
      operator WRAP&& ()       && {
        return reinterpret_cast<WRAP&&>(*this);
      }
      template <class WRAP, typename std::enable_if<details::valid_implicit_cast<const dc_wrap&,  WRAP& >() && !details::valid_implicit_cast<const dc_wrap&,  typename std::remove_cv<WRAP>::type>()>::type* = nullptr>
      operator WRAP&  () const &  {
        return reinterpret_cast<WRAP& >(*this);
      }
      template <class WRAP, typename std::enable_if<details::valid_implicit_cast<const dc_wrap&&, WRAP&&>() && !details::valid_implicit_cast<const dc_wrap&&, typename std::remove_cv<WRAP>::type>()>::type* = nullptr>
      operator WRAP&& () const && {
        return reinterpret_cast<WRAP&&>(*this);
      }

      // Explicit conversion to wrapper reference
      template <class WRAP, typename std::enable_if<details::valid_explicit_cast<      dc_wrap&,  WRAP& >() && !details::valid_cast<      dc_wrap&,  typename std::remove_cv<WRAP>::type>()>::type* = nullptr>
      explicit operator WRAP&  ()       & {
        return reinterpret_cast<WRAP& >(*this);
      }
      template <class WRAP, typename std::enable_if<details::valid_explicit_cast<      dc_wrap&&, WRAP&&>() && !details::valid_cast<      dc_wrap&&, typename std::remove_cv<WRAP>::type>()>::type* = nullptr>
      explicit operator WRAP&& ()       && {
        return reinterpret_cast<WRAP&&>(*this);
      }
      template <class WRAP, typename std::enable_if<details::valid_explicit_cast<const dc_wrap&,  WRAP& >() && !details::valid_cast<const dc_wrap&,  typename std::remove_cv<WRAP>::type>()>::type* = nullptr>
      explicit operator WRAP&  () const &  {
        return reinterpret_cast<WRAP& >(*this);
      }
      template <class WRAP, typename std::enable_if<details::valid_explicit_cast<const dc_wrap&&, WRAP&&>() && !details::valid_cast<const dc_wrap&&, typename std::remove_cv<WRAP>::type>()>::type* = nullptr>
      explicit operator WRAP&& () const && {
        return reinterpret_cast<WRAP&&>(*this);
      }

    public:
      /**
       * Dereferecing operators
       */
      auto operator*() const noexcept -> decltype((*(this->get()))) {
        return *(this->get());
      }
      auto operator*() noexcept -> decltype((*(this->get()))) {
        return *(this->get());
      }

      auto operator[](std::ptrdiff_t n) const noexcept -> decltype((this->get()[n])) {
        return this->get()[n];
      }
      auto operator[](std::ptrdiff_t n) noexcept -> decltype((this->get()[n])) {
        return this->get()[n];
      }

      auto operator->() const noexcept -> decltype(this->get()) {
        return this->get();
      }
      auto operator->() noexcept -> decltype(this->get()) {
        return this->get();
      }

      /**
       * Pointer arithmetic
       */
      const_t operator+(std::ptrdiff_t n) const noexcept {
        return const_t(this->get() + n);
      }
      dc_wrap operator+(std::ptrdiff_t n) noexcept {
        return dc_wrap(this->get() + n);
      }

      friend const_t operator+(std::ptrdiff_t n, const dc_wrap&  dc) noexcept {
        return dc + n;
      }
      friend dc_wrap operator+(std::ptrdiff_t n,       dc_wrap&  dc) noexcept {
        return dc + n;
      }
      friend dc_wrap operator+(std::ptrdiff_t n,       dc_wrap&& dc) noexcept {
        return dc + n;
      }

      const_t operator-(std::ptrdiff_t n) const noexcept {
        return const_t(this->get() - n);
      }
      dc_wrap operator-(std::ptrdiff_t n) noexcept {
        return dc_wrap(this->get() - n);
      }

      dc_wrap& operator+=(std::ptrdiff_t n) noexcept {
        this->p += n;
        return *this;
      }
      dc_wrap& operator-=(std::ptrdiff_t n) noexcept {
        this->p -= n;
        return *this;
      }

      std::ptrdiff_t operator-(const dc_wrap& dc) const noexcept {
        return dc.get() - this->get();
      }

      /**
       * Comparison ops
       */
      bool operator==(const dc_wrap& dc) const noexcept {
        return this->get() == dc.get();
      }
      bool operator!=(const dc_wrap& dc) const noexcept {
        return this->get() != dc.get();
      }
      bool operator<(const dc_wrap& dc) const noexcept {
        return this->get() < dc.get();
      }
      bool operator<=(const dc_wrap& dc) const noexcept {
        return this->get() <= dc.get();
      }
      bool operator>(const dc_wrap& dc) const noexcept {
        return this->get() > dc.get();
      }
      bool operator>=(const dc_wrap& dc) const noexcept {
        return this->get() >= dc.get();
      }

  }; // class dc_wrap<T, false>

} // namespace deep_const

#endif // DEEP_CONST_H

////////////////////////////////////////////////////////////////////////////////
// The MIT License                                                            //
//                                                                            //
// Copyright (c) 2017 Florian Lemaitre                                        //
//                                                                            //
// Permission is hereby granted, free of charge,                              //
// to any person obtaining a copy of this software and                        //
// associated documentation files (the "Software"), to                        //
// deal in the Software without restriction, including                        //
// without limitation the rights to use, copy, modify,                        //
// merge, publish, distribute, sublicense, and/or sell                        //
// copies of the Software, and to permit persons to whom                      //
// the Software is furnished to do so,                                        //
// subject to the following conditions:                                       //
//                                                                            //
// The above copyright notice and this permission notice                      //
// shall be included in all copies or substantial portions of the Software.   //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,            //
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES            //
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  //
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR           //
// ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,   //
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE          //
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                     //
////////////////////////////////////////////////////////////////////////////////
