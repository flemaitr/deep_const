#include "deep_const.h"
#include <type_traits>

using namespace deep_const;
using namespace deep_const::details;


template <class TO>
constexpr std::true_type  func_caster(TO)  { return {}; }
template <class TO>
constexpr std::false_type func_caster(...) { return {}; }


template <class FROM, class TO>
struct test_cast_construct {
  using TO_t = typename std::remove_reference<TO>::type;
  static constexpr bool verify(...) { return std::is_constructible<TO, FROM>::value; }
  //template <decltype(TO_t(declval<FROM>()), nullptr) = nullptr>
  //static constexpr bool verify(int) { return true; }
  //static constexpr bool verify(...) { return false; }
};

template <class FROM, class TO>
struct test_cast_assign {
  template <decltype(declval<TO&>() = declval<FROM>(), nullptr) = nullptr>
  static constexpr bool verify(int) { return true; }
  static constexpr bool verify(...) { return false; }
};

template <class FROM, class TO>
struct test_cast_static {
  template <decltype(static_cast<TO>(declval<FROM>()), nullptr) = nullptr>
  static constexpr bool verify(int) { return true; }
  static constexpr bool verify(...) { return false; }
};

template <class FROM, class TO>
struct test_cast_call {
  template <decltype(func_caster<TO>(declval<FROM>()), nullptr) = nullptr>
  static constexpr bool verify(int) { return decltype(func_caster<TO>(declval<FROM>()))::value; }
  static constexpr bool verify(...) { return false; }
};

template <class FROM, class TO>
struct test_cast_convertible {
  static constexpr bool verify(int) { return std::is_convertible<FROM, TO>::value; }
};

template <class FROM, class TO>
constexpr bool is_not_convertible() {
  return !test_cast_construct  <FROM, TO>::verify(0)
      && !test_cast_assign     <FROM, TO>::verify(0)
      && !test_cast_static     <FROM, TO>::verify(0)
      && !test_cast_call       <FROM, TO>::verify(0)
      && !test_cast_convertible<FROM, TO>::verify(0)
  ;
}
template <class FROM, class TO>
constexpr bool is_explicitely_convertible() {
  return  test_cast_construct  <FROM, TO>::verify(0)
      && !test_cast_assign     <FROM, TO>::verify(0)
      &&  test_cast_static     <FROM, TO>::verify(0)
      && !test_cast_call       <FROM, TO>::verify(0)
      && !test_cast_convertible<FROM, TO>::verify(0)
  ;
}
template <class FROM, class TO>
constexpr bool is_implicitely_convertible() {
  return  test_cast_construct  <FROM, TO>::verify(0)
      &&  test_cast_assign     <FROM, TO>::verify(0)
      &&  test_cast_static     <FROM, TO>::verify(0)
      &&  test_cast_call       <FROM, TO>::verify(0)
      &&  test_cast_convertible<FROM, TO>::verify(0)
  ;
}


template <class T>
struct remove_dc_s {
  using type = T;
};
template <class T>
struct remove_dc_s<const T> {
  using type = const typename remove_dc_s<T>::type;
};

template <class T>
struct remove_dc_s<T&> {
  using type = typename remove_dc_s<T>::type&;
};
template <class T>
struct remove_dc_s<T&&> {
  using type = typename remove_dc_s<T>::type&;
};
template <class T>
struct remove_dc_s<dc_wrap<T, false>> {
  using type = T;
};
template <class T>
struct remove_dc_s<const dc_wrap<T, false>> {
  using type = const deepen_const_t<T>;
};
template <class T>
struct remove_dc_s<dc_wrap<T, true>> {
  using type = deepen_const_t<T>;
};

template <class T>
using remove_dc = typename remove_dc_s<T>::type;



const char* types() { return __PRETTY_FUNCTION__; }
template <class... Args>
const char* types() { return __PRETTY_FUNCTION__; }
template <class... Args>
const char* types(Args&&...) { return types<Args&&...>(); }





#include <iostream>
#include <iomanip>

template <class FROM, class TO>
void check_casts() {
  std::cout << __PRETTY_FUNCTION__ << "\n";
  std::cout << types<FROM, TO>() << "\n";
  std::cout << types<remove_dc<FROM>, remove_dc<TO>>() << "\n";
  std::cout  << "    construct: " << test_cast_construct  <FROM, TO>::verify(0) << "\t" << test_cast_construct  <remove_dc<FROM>, remove_dc<TO>>::verify(0) << "\n";
  std::cout  << "       assign: " << test_cast_assign     <FROM, TO>::verify(0) << "\t" << test_cast_assign     <remove_dc<FROM>, remove_dc<TO>>::verify(0) << "\n";
  std::cout  << "       static: " << test_cast_static     <FROM, TO>::verify(0) << "\t" << test_cast_static     <remove_dc<FROM>, remove_dc<TO>>::verify(0) << "\n";
  std::cout  << "         call: " << test_cast_call       <FROM, TO>::verify(0) << "\t" << test_cast_call       <remove_dc<FROM>, remove_dc<TO>>::verify(0) << "\n";
  std::cout  << "  convertible: " << test_cast_convertible<FROM, TO>::verify(0) << "\t" << test_cast_convertible<remove_dc<FROM>, remove_dc<TO>>::verify(0) << "\n";
  std::cout << std::endl;
}

template <class T> using   const_wrap = dc_wrap<T, true>;
template <class T> using mutable_wrap = dc_wrap<T, false>;

struct Base {};
struct Derived : public Base {};

//template <class FROM, class TO>
//constexpr bool same_cast_rules_as_pointer() {
//  return test_cast_construct  <FROM, TO>::verify(0) == test_cast_construct  <remove_dc<FROM>, remove_dc<TO>>::verify(0)
//      && test_cast_assign     <FROM, TO>::verify(0) == test_cast_assign     <remove_dc<FROM>, remove_dc<TO>>::verify(0)
//      && test_cast_static     <FROM, TO>::verify(0) == test_cast_static     <remove_dc<FROM>, remove_dc<TO>>::verify(0)
//      && test_cast_call       <FROM, TO>::verify(0) == test_cast_call       <remove_dc<FROM>, remove_dc<TO>>::verify(0)
//      && test_cast_convertible<FROM, TO>::verify(0) == test_cast_convertible<remove_dc<FROM>, remove_dc<TO>>::verify(0)
//  ;
//}
template <class FROM, class TO>
void  assert_same_cast_rules_as_pointer_except_construct() {
  static_assert(!test_cast_construct  <FROM, TO>::verify(0) || test_cast_construct  <remove_dc<FROM>, remove_dc<TO>>::verify(0), "Cast rules differ from pointers");
  static_assert( test_cast_assign     <FROM, TO>::verify(0) == test_cast_assign     <remove_dc<FROM>, remove_dc<TO>>::verify(0), "Cast rules differ from pointers");
  static_assert( test_cast_static     <FROM, TO>::verify(0) == test_cast_static     <remove_dc<FROM>, remove_dc<TO>>::verify(0), "Cast rules differ from pointers");
  static_assert( test_cast_call       <FROM, TO>::verify(0) == test_cast_call       <remove_dc<FROM>, remove_dc<TO>>::verify(0), "Cast rules differ from pointers");
  static_assert( test_cast_convertible<FROM, TO>::verify(0) == test_cast_convertible<remove_dc<FROM>, remove_dc<TO>>::verify(0), "Cast rules differ from pointers");
}
template <class FROM, class TO>
void  assert_same_cast_rules_as_pointer_explicit_construct() {
  constexpr bool  dc_construct   = test_cast_construct  <FROM, TO>::verify(0);
  constexpr bool ptr_construct   = test_cast_construct  <remove_dc<FROM>, remove_dc<TO>>::verify(0);
  constexpr bool  dc_static      = test_cast_static     <FROM, TO>::verify(0);
  //constexpr bool ptr_static      = test_cast_static     <remove_dc<FROM>, remove_dc<TO>>::verify(0);
  static_assert( !dc_static || dc_construct || (dc_construct == ptr_construct), "Cast rules differ from pointers");
  static_assert( test_cast_assign     <FROM, TO>::verify(0) == test_cast_assign     <remove_dc<FROM>, remove_dc<TO>>::verify(0), "Cast rules differ from pointers");
  static_assert( test_cast_static     <FROM, TO>::verify(0) == test_cast_static     <remove_dc<FROM>, remove_dc<TO>>::verify(0), "Cast rules differ from pointers");
  static_assert( test_cast_call       <FROM, TO>::verify(0) == test_cast_call       <remove_dc<FROM>, remove_dc<TO>>::verify(0), "Cast rules differ from pointers");
  static_assert( test_cast_convertible<FROM, TO>::verify(0) == test_cast_convertible<remove_dc<FROM>, remove_dc<TO>>::verify(0), "Cast rules differ from pointers");
}
template <class FROM, class TO>
void  assert_same_cast_rules_as_pointer() {
  assert_same_cast_rules_as_pointer_explicit_construct<FROM, TO>();
  //static_assert( test_cast_construct  <FROM, TO>::verify(0) == test_cast_construct  <remove_dc<FROM>, remove_dc<TO>>::verify(0), "Cast rules differ from pointers");
  //static_assert( test_cast_assign     <FROM, TO>::verify(0) == test_cast_assign     <remove_dc<FROM>, remove_dc<TO>>::verify(0), "Cast rules differ from pointers");
  //static_assert( test_cast_static     <FROM, TO>::verify(0) == test_cast_static     <remove_dc<FROM>, remove_dc<TO>>::verify(0), "Cast rules differ from pointers");
  //static_assert( test_cast_call       <FROM, TO>::verify(0) == test_cast_call       <remove_dc<FROM>, remove_dc<TO>>::verify(0), "Cast rules differ from pointers");
  //static_assert( test_cast_convertible<FROM, TO>::verify(0) == test_cast_convertible<remove_dc<FROM>, remove_dc<TO>>::verify(0), "Cast rules differ from pointers");
}

template <class T>
constexpr bool is_const() { return std::is_const<T>::value; }

template <class FROM, class TO>
void foo() {
  static_assert(is_const<TO>() || is_not_convertible<        const_wrap<FROM*>  , mutable_wrap<TO*>  >(), "constness leaking");
  static_assert(is_const<TO>() || is_not_convertible<        const_wrap<FROM*>  , mutable_wrap<TO*>& >(), "constness leaking");
  static_assert(is_const<TO>() || is_not_convertible<        const_wrap<FROM*>  , mutable_wrap<TO*>&&>(), "constness leaking");
  static_assert(is_const<TO>() || is_not_convertible<const   const_wrap<FROM*>  , mutable_wrap<TO*>  >(), "constness leaking");
  static_assert(is_const<TO>() || is_not_convertible<const   const_wrap<FROM*>  , mutable_wrap<TO*>& >(), "constness leaking");
  static_assert(is_const<TO>() || is_not_convertible<const   const_wrap<FROM*>  , mutable_wrap<TO*>&&>(), "constness leaking");
  static_assert(is_const<TO>() || is_not_convertible<const mutable_wrap<FROM*>  , mutable_wrap<TO*>  >(), "constness leaking");
  static_assert(is_const<TO>() || is_not_convertible<const mutable_wrap<FROM*>  , mutable_wrap<TO*>& >(), "constness leaking");
  static_assert(is_const<TO>() || is_not_convertible<const mutable_wrap<FROM*>  , mutable_wrap<TO*>&&>(), "constness leaking");
  static_assert(is_const<TO>() || is_not_convertible<        const_wrap<FROM*>& , mutable_wrap<TO*>  >(), "constness leaking");
  static_assert(is_const<TO>() || is_not_convertible<        const_wrap<FROM*>& , mutable_wrap<TO*>& >(), "constness leaking");
  static_assert(is_const<TO>() || is_not_convertible<        const_wrap<FROM*>& , mutable_wrap<TO*>&&>(), "constness leaking");
  static_assert(is_const<TO>() || is_not_convertible<        const_wrap<FROM*>&&, mutable_wrap<TO*>  >(), "constness leaking");
  static_assert(is_const<TO>() || is_not_convertible<        const_wrap<FROM*>&&, mutable_wrap<TO*>& >(), "constness leaking");
  static_assert(is_const<TO>() || is_not_convertible<        const_wrap<FROM*>&&, mutable_wrap<TO*>&&>(), "constness leaking");
  static_assert(is_const<TO>() || is_not_convertible<const   const_wrap<FROM*>& , mutable_wrap<TO*>  >(), "constness leaking");
  static_assert(is_const<TO>() || is_not_convertible<const   const_wrap<FROM*>& , mutable_wrap<TO*>& >(), "constness leaking");
  static_assert(is_const<TO>() || is_not_convertible<const   const_wrap<FROM*>& , mutable_wrap<TO*>&&>(), "constness leaking");
  static_assert(is_const<TO>() || is_not_convertible<const   const_wrap<FROM*>&&, mutable_wrap<TO*>  >(), "constness leaking");
  static_assert(is_const<TO>() || is_not_convertible<const   const_wrap<FROM*>&&, mutable_wrap<TO*>& >(), "constness leaking");
  static_assert(is_const<TO>() || is_not_convertible<const   const_wrap<FROM*>&&, mutable_wrap<TO*>&&>(), "constness leaking");
  static_assert(is_const<TO>() || is_not_convertible<const mutable_wrap<FROM*>& , mutable_wrap<TO*>  >(), "constness leaking");
  static_assert(is_const<TO>() || is_not_convertible<const mutable_wrap<FROM*>& , mutable_wrap<TO*>& >(), "constness leaking");
  static_assert(is_const<TO>() || is_not_convertible<const mutable_wrap<FROM*>& , mutable_wrap<TO*>&&>(), "constness leaking");
  static_assert(is_const<TO>() || is_not_convertible<const mutable_wrap<FROM*>&&, mutable_wrap<TO*>  >(), "constness leaking");
  static_assert(is_const<TO>() || is_not_convertible<const mutable_wrap<FROM*>&&, mutable_wrap<TO*>& >(), "constness leaking");
  static_assert(is_const<TO>() || is_not_convertible<const mutable_wrap<FROM*>&&, mutable_wrap<TO*>&&>(), "constness leaking");

  assert_same_cast_rules_as_pointer<        const_wrap<FROM*>  ,       const_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer<      mutable_wrap<FROM*>  ,       const_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer<        const_wrap<FROM*>  ,       const_wrap<TO*>& >();
  assert_same_cast_rules_as_pointer<      mutable_wrap<FROM*>  ,       const_wrap<TO*>& >();
  //assert_same_cast_rules_as_pointer<        const_wrap<FROM*>  ,       const_wrap<TO*>&&>();
  //assert_same_cast_rules_as_pointer<      mutable_wrap<FROM*>  ,       const_wrap<TO*>&&>();
  assert_same_cast_rules_as_pointer<        const_wrap<FROM*>  , const const_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer<      mutable_wrap<FROM*>  , const const_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer<        const_wrap<FROM*>  , const const_wrap<TO*>& >();
  assert_same_cast_rules_as_pointer<      mutable_wrap<FROM*>  , const const_wrap<TO*>& >();
  assert_same_cast_rules_as_pointer<        const_wrap<FROM*>  , const const_wrap<TO*>&&>();
  assert_same_cast_rules_as_pointer<      mutable_wrap<FROM*>  , const const_wrap<TO*>&&>();
  assert_same_cast_rules_as_pointer<        const_wrap<FROM*>& ,       const_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer<      mutable_wrap<FROM*>& ,       const_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer<        const_wrap<FROM*>& ,       const_wrap<TO*>& >();
#ifndef __INTEL_COMPILER
  assert_same_cast_rules_as_pointer<      mutable_wrap<FROM*>& ,       const_wrap<TO*>& >();
#else
  static_assert(
          test_cast_construct<mutable_wrap<FROM*>&, const_wrap<TO*>&>::verify(0)
      ==  test_cast_static   <mutable_wrap<FROM*>&, const_wrap<TO*>&>::verify(0)
      //==  test_cast_construct<remove_dc<mutable_wrap<FROM*>&>, remove_dc<const_wrap<TO*>&>>::verify(0)
      , "");
  static_assert(
          test_cast_assign<mutable_wrap<FROM*>&, const_wrap<TO*>&>::verify(0)
      ==  test_cast_assign<remove_dc<mutable_wrap<FROM*>&>, remove_dc<const_wrap<TO*>&>>::verify(0)
      , "");
  static_assert(
          test_cast_call<mutable_wrap<FROM*>&, const_wrap<TO*>&>::verify(0)
      ==  test_cast_call<remove_dc<mutable_wrap<FROM*>&>, remove_dc<const_wrap<TO*>&>>::verify(0)
      , "");
  static_assert(
          test_cast_convertible<mutable_wrap<FROM*>&, const_wrap<TO*>&>::verify(0)
      ==  test_cast_convertible<remove_dc<mutable_wrap<FROM*>&>, remove_dc<const_wrap<TO*>&>>::verify(0)
      , "");
#endif
  //assert_same_cast_rules_as_pointer<        const_wrap<FROM*>& ,       const_wrap<TO*>&&>();
  //assert_same_cast_rules_as_pointer<      mutable_wrap<FROM*>& ,       const_wrap<TO*>&&>();
  assert_same_cast_rules_as_pointer<        const_wrap<FROM*>& , const const_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer<      mutable_wrap<FROM*>& , const const_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer<        const_wrap<FROM*>& , const const_wrap<TO*>& >();
  assert_same_cast_rules_as_pointer<      mutable_wrap<FROM*>& , const const_wrap<TO*>& >();
  //assert_same_cast_rules_as_pointer<        const_wrap<FROM*>& , const const_wrap<TO*>&&>();
  //assert_same_cast_rules_as_pointer<      mutable_wrap<FROM*>& , const const_wrap<TO*>&&>();
  assert_same_cast_rules_as_pointer<        const_wrap<FROM*>&&,       const_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer<      mutable_wrap<FROM*>&&,       const_wrap<TO*>  >();
  //assert_same_cast_rules_as_pointer<        const_wrap<FROM*>&&,       const_wrap<TO*>& >();
#ifndef __INTEL_COMPILER
  assert_same_cast_rules_as_pointer<      mutable_wrap<FROM*>&&,       const_wrap<TO*>& >();
#else
  static_assert(
          test_cast_construct<mutable_wrap<FROM*>&&, const_wrap<TO*>&>::verify(0)
      ==  test_cast_static   <mutable_wrap<FROM*>&&, const_wrap<TO*>&>::verify(0)
      //==  test_cast_construct<remove_dc<mutable_wrap<FROM*>&&>, remove_dc<const_wrap<TO*>&>>::verify(0)
      , "");
  static_assert(
          test_cast_assign<mutable_wrap<FROM*>&&, const_wrap<TO*>&>::verify(0)
      ==  test_cast_assign<remove_dc<mutable_wrap<FROM*>&&>, remove_dc<const_wrap<TO*>&>>::verify(0)
      , "");
  static_assert(
          test_cast_call<mutable_wrap<FROM*>&&, const_wrap<TO*>&>::verify(0)
      ==  test_cast_call<remove_dc<mutable_wrap<FROM*>&&>, remove_dc<const_wrap<TO*>&>>::verify(0)
      , "");
  static_assert(
          test_cast_convertible<mutable_wrap<FROM*>&&, const_wrap<TO*>&>::verify(0)
      ==  test_cast_convertible<remove_dc<mutable_wrap<FROM*>&&>, remove_dc<const_wrap<TO*>&>>::verify(0)
      , "");
#endif
  //assert_same_cast_rules_as_pointer<        const_wrap<FROM*>&&,       const_wrap<TO*>&&>();
  //assert_same_cast_rules_as_pointer<      mutable_wrap<FROM*>&&,       const_wrap<TO*>&&>();
  assert_same_cast_rules_as_pointer<        const_wrap<FROM*>&&, const const_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer<      mutable_wrap<FROM*>&&, const const_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer<        const_wrap<FROM*>&&, const const_wrap<TO*>& >();
  assert_same_cast_rules_as_pointer<      mutable_wrap<FROM*>&&, const const_wrap<TO*>& >();
  assert_same_cast_rules_as_pointer<        const_wrap<FROM*>&&, const const_wrap<TO*>&&>();
  assert_same_cast_rules_as_pointer<      mutable_wrap<FROM*>&&, const const_wrap<TO*>&&>();
  assert_same_cast_rules_as_pointer<const   const_wrap<FROM*>  ,       const_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer<const mutable_wrap<FROM*>  ,       const_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer<const   const_wrap<FROM*>  ,       const_wrap<TO*>& >();
  assert_same_cast_rules_as_pointer<const mutable_wrap<FROM*>  ,       const_wrap<TO*>& >();
  //assert_same_cast_rules_as_pointer<const   const_wrap<FROM*>  ,       const_wrap<TO*>&&>();
  //assert_same_cast_rules_as_pointer<const mutable_wrap<FROM*>  ,       const_wrap<TO*>&&>();
  assert_same_cast_rules_as_pointer<const   const_wrap<FROM*>  , const const_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer<const mutable_wrap<FROM*>  , const const_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer<const   const_wrap<FROM*>  , const const_wrap<TO*>& >();
  assert_same_cast_rules_as_pointer<const mutable_wrap<FROM*>  , const const_wrap<TO*>& >();
  assert_same_cast_rules_as_pointer<const   const_wrap<FROM*>  , const const_wrap<TO*>&&>();
  assert_same_cast_rules_as_pointer<const mutable_wrap<FROM*>  , const const_wrap<TO*>&&>();
  assert_same_cast_rules_as_pointer<const   const_wrap<FROM*>& ,       const_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer<const mutable_wrap<FROM*>& ,       const_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer<const   const_wrap<FROM*>& ,       const_wrap<TO*>& >();
  assert_same_cast_rules_as_pointer<const mutable_wrap<FROM*>& ,       const_wrap<TO*>& >();
  //assert_same_cast_rules_as_pointer<const   const_wrap<FROM*>& ,       const_wrap<TO*>&&>();
  //assert_same_cast_rules_as_pointer<const mutable_wrap<FROM*>& ,       const_wrap<TO*>&&>();
  assert_same_cast_rules_as_pointer<const   const_wrap<FROM*>& , const const_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer<const mutable_wrap<FROM*>& , const const_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer<const   const_wrap<FROM*>& , const const_wrap<TO*>& >();
  assert_same_cast_rules_as_pointer<const mutable_wrap<FROM*>& , const const_wrap<TO*>& >();
  //assert_same_cast_rules_as_pointer<const   const_wrap<FROM*>& , const const_wrap<TO*>&&>();
  //assert_same_cast_rules_as_pointer<const mutable_wrap<FROM*>& , const const_wrap<TO*>&&>();
  assert_same_cast_rules_as_pointer<const   const_wrap<FROM*>&&,       const_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer<const mutable_wrap<FROM*>&&,       const_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer<const   const_wrap<FROM*>&&,       const_wrap<TO*>& >();
  assert_same_cast_rules_as_pointer<const mutable_wrap<FROM*>&&,       const_wrap<TO*>& >();
  //assert_same_cast_rules_as_pointer<const   const_wrap<FROM*>&&,       const_wrap<TO*>&&>();
  //assert_same_cast_rules_as_pointer<const mutable_wrap<FROM*>&&,       const_wrap<TO*>&&>();
  assert_same_cast_rules_as_pointer<const   const_wrap<FROM*>&&, const const_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer<const mutable_wrap<FROM*>&&, const const_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer<const   const_wrap<FROM*>&&, const const_wrap<TO*>& >();
  assert_same_cast_rules_as_pointer<const mutable_wrap<FROM*>&&, const const_wrap<TO*>& >();
  assert_same_cast_rules_as_pointer<const   const_wrap<FROM*>&&, const const_wrap<TO*>&&>();
  assert_same_cast_rules_as_pointer<const mutable_wrap<FROM*>&&, const const_wrap<TO*>&&>();

  // These should be well defined but it is impossible to make them valid while keeping the constness-leak freedom
  //assert_same_cast_rules_as_pointer_except_construct<        const_wrap<FROM*>  , const mutable_wrap<TO*>  >();
  //assert_same_cast_rules_as_pointer_except_construct<        const_wrap<FROM*>& , const mutable_wrap<TO*>  >();
  //assert_same_cast_rules_as_pointer_except_construct<        const_wrap<FROM*>&&, const mutable_wrap<TO*>  >();
  //assert_same_cast_rules_as_pointer_except_construct<const   const_wrap<FROM*>  , const mutable_wrap<TO*>  >();
  //assert_same_cast_rules_as_pointer_except_construct<const mutable_wrap<FROM*>  , const mutable_wrap<TO*>  >();
  //assert_same_cast_rules_as_pointer_except_construct<const   const_wrap<FROM*>& , const mutable_wrap<TO*>  >();
  //assert_same_cast_rules_as_pointer_except_construct<const mutable_wrap<FROM*>& , const mutable_wrap<TO*>  >();
  //assert_same_cast_rules_as_pointer_except_construct<const   const_wrap<FROM*>&&, const mutable_wrap<TO*>  >();
  //assert_same_cast_rules_as_pointer_except_construct<const mutable_wrap<FROM*>&&, const mutable_wrap<TO*>  >();


//  assert_same_cast_rules_as_pointer_except_construct<      mutable_wrap<FROM*>  , const mutable_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer_except_construct<        const_wrap<FROM*>  , const mutable_wrap<TO*>& >();
  assert_same_cast_rules_as_pointer_except_construct<      mutable_wrap<FROM*>  , const mutable_wrap<TO*>& >();
  assert_same_cast_rules_as_pointer_except_construct<        const_wrap<FROM*>  , const mutable_wrap<TO*>&&>();
  assert_same_cast_rules_as_pointer_except_construct<      mutable_wrap<FROM*>  , const mutable_wrap<TO*>&&>();
//  assert_same_cast_rules_as_pointer_except_construct<      mutable_wrap<FROM*>& , const mutable_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer_except_construct<        const_wrap<FROM*>& , const mutable_wrap<TO*>& >();
  assert_same_cast_rules_as_pointer_except_construct<      mutable_wrap<FROM*>& , const mutable_wrap<TO*>& >();
  //assert_same_cast_rules_as_pointer_except_construct<        const_wrap<FROM*>& , const mutable_wrap<TO*>&&>();
  //assert_same_cast_rules_as_pointer_except_construct<      mutable_wrap<FROM*>& , const mutable_wrap<TO*>&&>();
//  assert_same_cast_rules_as_pointer_except_construct<      mutable_wrap<FROM*>&&, const mutable_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer_except_construct<        const_wrap<FROM*>&&, const mutable_wrap<TO*>& >();
  assert_same_cast_rules_as_pointer_except_construct<      mutable_wrap<FROM*>&&, const mutable_wrap<TO*>& >();
  assert_same_cast_rules_as_pointer_except_construct<        const_wrap<FROM*>&&, const mutable_wrap<TO*>&&>();
  assert_same_cast_rules_as_pointer_except_construct<      mutable_wrap<FROM*>&&, const mutable_wrap<TO*>&&>();
  assert_same_cast_rules_as_pointer_except_construct<const   const_wrap<FROM*>  , const mutable_wrap<TO*>& >();
  assert_same_cast_rules_as_pointer_except_construct<const mutable_wrap<FROM*>  , const mutable_wrap<TO*>& >();
  assert_same_cast_rules_as_pointer_except_construct<const   const_wrap<FROM*>  , const mutable_wrap<TO*>&&>();
  assert_same_cast_rules_as_pointer_except_construct<const mutable_wrap<FROM*>  , const mutable_wrap<TO*>&&>();
  assert_same_cast_rules_as_pointer_except_construct<const   const_wrap<FROM*>& , const mutable_wrap<TO*>& >();
  assert_same_cast_rules_as_pointer_except_construct<const mutable_wrap<FROM*>& , const mutable_wrap<TO*>& >();
  //assert_same_cast_rules_as_pointer_except_construct<const   const_wrap<FROM*>& , const mutable_wrap<TO*>&&>();
  //assert_same_cast_rules_as_pointer_except_construct<const mutable_wrap<FROM*>& , const mutable_wrap<TO*>&&>();
  assert_same_cast_rules_as_pointer_except_construct<const   const_wrap<FROM*>&&, const mutable_wrap<TO*>& >();
  assert_same_cast_rules_as_pointer_except_construct<const mutable_wrap<FROM*>&&, const mutable_wrap<TO*>& >();
  assert_same_cast_rules_as_pointer_except_construct<const   const_wrap<FROM*>&&, const mutable_wrap<TO*>&&>();
  assert_same_cast_rules_as_pointer_except_construct<const mutable_wrap<FROM*>&&, const mutable_wrap<TO*>&&>();

  assert_same_cast_rules_as_pointer<mutable_wrap<FROM*>  ,       mutable_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer<mutable_wrap<FROM*>  ,       mutable_wrap<TO*>& >();
  //assert_same_cast_rules_as_pointer<mutable_wrap<FROM*>  ,       mutable_wrap<TO*>&&>();
  assert_same_cast_rules_as_pointer<mutable_wrap<FROM*>  , const mutable_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer<mutable_wrap<FROM*>  , const mutable_wrap<TO*>& >();
  assert_same_cast_rules_as_pointer<mutable_wrap<FROM*>  , const mutable_wrap<TO*>&&>();
  assert_same_cast_rules_as_pointer<mutable_wrap<FROM*>& ,       mutable_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer<mutable_wrap<FROM*>& ,       mutable_wrap<TO*>& >();
  //assert_same_cast_rules_as_pointer<mutable_wrap<FROM*>& ,       mutable_wrap<TO*>&&>();
  assert_same_cast_rules_as_pointer<mutable_wrap<FROM*>& , const mutable_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer<mutable_wrap<FROM*>& , const mutable_wrap<TO*>& >();
  //assert_same_cast_rules_as_pointer<mutable_wrap<FROM*>& , const mutable_wrap<TO*>&&>();
  assert_same_cast_rules_as_pointer<mutable_wrap<FROM*>&&,       mutable_wrap<TO*>  >();
  //assert_same_cast_rules_as_pointer<mutable_wrap<FROM*>&&,       mutable_wrap<TO*>& >();
  //assert_same_cast_rules_as_pointer<mutable_wrap<FROM*>&&,       mutable_wrap<TO*>&&>();
  assert_same_cast_rules_as_pointer<mutable_wrap<FROM*>&&, const mutable_wrap<TO*>  >();
  assert_same_cast_rules_as_pointer<mutable_wrap<FROM*>&&, const mutable_wrap<TO*>& >();
  assert_same_cast_rules_as_pointer<mutable_wrap<FROM*>&&, const mutable_wrap<TO*>&&>();
}

template <class FROM, class TO>
void assert_no_valid_cast() {
  static_assert(is_not_convertible<        const_wrap<FROM*>  ,         const_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>  ,       mutable_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>  ,         const_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>  ,       mutable_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>  ,         const_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>  ,       mutable_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>  ,         const_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>  ,       mutable_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>  ,         const_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>  ,       mutable_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>  ,         const_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>  ,       mutable_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>  , const   const_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>  , const mutable_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>  , const   const_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>  , const mutable_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>  , const   const_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>  , const mutable_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>  , const   const_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>  , const mutable_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>  , const   const_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>  , const mutable_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>  , const   const_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>  , const mutable_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>& ,         const_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>& ,       mutable_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>& ,         const_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>& ,       mutable_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>& ,         const_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>& ,       mutable_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>& ,         const_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>& ,       mutable_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>& ,         const_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>& ,       mutable_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>& ,         const_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>& ,       mutable_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>& , const   const_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>& , const mutable_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>& , const   const_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>& , const mutable_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>& , const   const_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>& , const mutable_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>& , const   const_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>& , const mutable_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>& , const   const_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>& , const mutable_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>& , const   const_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>& , const mutable_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>&&,         const_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>&&,       mutable_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>&&,         const_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>&&,       mutable_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>&&,         const_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>&&,       mutable_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>&&,         const_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>&&,       mutable_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>&&,         const_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>&&,       mutable_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>&&,         const_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>&&,       mutable_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>&&, const   const_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>&&, const mutable_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>&&, const   const_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>&&, const mutable_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>&&, const   const_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>&&, const mutable_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>&&, const   const_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>&&, const mutable_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>&&, const   const_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<        const_wrap<FROM*>&&, const mutable_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>&&, const   const_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<      mutable_wrap<FROM*>&&, const mutable_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>  ,         const_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>  ,       mutable_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>  ,         const_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>  ,       mutable_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>  ,         const_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>  ,       mutable_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>  ,         const_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>  ,       mutable_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>  ,         const_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>  ,       mutable_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>  ,         const_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>  ,       mutable_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>  , const   const_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>  , const mutable_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>  , const   const_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>  , const mutable_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>  , const   const_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>  , const mutable_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>  , const   const_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>  , const mutable_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>  , const   const_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>  , const mutable_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>  , const   const_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>  , const mutable_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>& ,         const_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>& ,       mutable_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>& ,         const_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>& ,       mutable_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>& ,         const_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>& ,       mutable_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>& ,         const_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>& ,       mutable_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>& ,         const_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>& ,       mutable_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>& ,         const_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>& ,       mutable_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>& , const   const_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>& , const mutable_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>& , const   const_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>& , const mutable_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>& , const   const_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>& , const mutable_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>& , const   const_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>& , const mutable_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>& , const   const_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>& , const mutable_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>& , const   const_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>& , const mutable_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>&&,         const_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>&&,       mutable_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>&&,         const_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>&&,       mutable_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>&&,         const_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>&&,       mutable_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>&&,         const_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>&&,       mutable_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>&&,         const_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>&&,       mutable_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>&&,         const_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>&&,       mutable_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>&&, const   const_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>&&, const mutable_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>&&, const   const_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>&&, const mutable_wrap<TO*>  >(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>&&, const   const_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>&&, const mutable_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>&&, const   const_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>&&, const mutable_wrap<TO*>& >(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>&&, const   const_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<const   const_wrap<FROM*>&&, const mutable_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>&&, const   const_wrap<TO*>&&>(), "");
  static_assert(is_not_convertible<const mutable_wrap<FROM*>&&, const mutable_wrap<TO*>&&>(), "");
}


template <class T>
void qwerty(T) {}

//const_wrap<Base*> a(const_wrap<Derived*>& w) {
//  return w;
//}
//const_wrap<Base*>& b(const_wrap<Derived*>& w) {
//  return w;
//}
int main() {
  std::cout << std::boolalpha;
  //foo<int, int>();
  //foo<int, const int>();
  //foo<const int, int>();
  //assert_no_valid_cast<int, unsigned>();
  //assert_no_valid_cast<unsigned, int>();
  //foo<Base, Derived>();
  //foo<Derived, Base>();

  check_casts<const_wrap<int*>, mutable_wrap<int*>>();
  check_casts<const mutable_wrap<int*>, mutable_wrap<int*>>();
  check_casts<mutable_wrap<const int*>&&, mutable_wrap<int*>>();


  //decltype(static_cast<const_wrap<int*>&>(std::declval<const_wrap<const int*>&>()), nullptr) pouet = nullptr;

  return 0;
}
